package main

import (
  "net"
  "fmt"
  "errors"
  "net/http"
  "github.com/mitchellh/go-ps"
)


func isCharlesRunning() bool {
  allProcesses, err := ps.Processes()
  if err != nil {
    return false
  }
  for i := range allProcesses {
    if allProcesses[i].Executable() == "Charles" {
      return true
    }
  }
  return false
}


func externalIP() (string, error) {
  ifaces, err := net.Interfaces()
  if err != nil {
    return "", err
  }
  for _, iface := range ifaces {
    if iface.Flags&net.FlagUp == 0 {
      // interface down
      continue
    }
    if iface.Flags&net.FlagLoopback != 0 {
      // loopback interface
      continue
    }
    addrs, err := iface.Addrs()
    if err != nil {
      return "", err
    }
    for _, addr := range addrs {
      var ip net.IP
      switch v := addr.(type) {
      case *net.IPNet:
        ip = v.IP
      case *net.IPAddr:
        ip = v.IP
      }
      if ip == nil || ip.IsLoopback() {
        continue
      }
      ip = ip.To4()
      if ip == nil {
        // not an ipv4 address
        continue
      }
      return ip.String(), nil
    }
  }
  return "", errors.New("are you connected to the network?")
}


func renderProxyPacFile(ip string, port int) (string) {
return fmt.Sprintf(`function FindProxyForURL(url, host) {
  return "PROXY %s:%d";
}
`, ip, port)
}


func renderDirectPacFile() (string) {
return `function FindProxyForURL(url, host) {
  return "DIRECT";
}
`
}


func handler(w http.ResponseWriter, r *http.Request) {
  if isCharlesRunning() {
    ip, err := externalIP()
    if err != nil {
      panic(err)
    }
    fmt.Fprintf(w, renderProxyPacFile(ip, 8888))
  } else {
    fmt.Fprintf(w, renderDirectPacFile())
  }
}


func main() {
  ip, err := externalIP()
  if err != nil {
    panic(err)
  }
  fmt.Println(fmt.Sprintf(`http://%s:8080`, ip))
  http.HandleFunc("/", handler)
  http.ListenAndServe(":8080", nil)
}
