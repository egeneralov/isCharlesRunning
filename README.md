# isCharlesRunning?

But aren’t you tired of manually climbing into the network settings on your phone to enable or disable the use of proxies?

This utility determines if [Charles Proxy](https://www.charlesproxy.com/) is running on the system (tested on darwin) and gives [.pac] (https://en.wikipedia.org/wiki/Proxy_auto-config) the file to the client device. Now I just need to enter `http://192.168.88.252:8080` in the proxy autoconfiguration field once and the phone itself will understand when to give it traffic.

If the process `Charles` is not found in the system, then the configuration will be as follows:

    function FindProxyForURL (url, host) {
      return "DIRECT";
    }

And when will it be launched:

    function FindProxyForURL (url, host) {
      if (url.startsWith ('https:')) {
        return "PROXY 192.168.88.252:8888";
      } else {
        return "PROXY 192.168.88.252:8888";
      }
    }


* Instead of `192.168.88.252` you will see your address on the local network

## Installation

### Steps

- download (or build) binary as `/usr/local/bin/isCharlesRunning`
- write `~/Library/LaunchAgents/org.generalov.isCharlesRunning.plist` file (contents below)
- start service by running `launchctl load -w ~/Library/LaunchAgents/org.generalov.isCharlesRunning.plist`
- test is service runnig via `curl -s http://192.168.88.252:8080`
- run charles, test again

### org.generalov.isCharlesRunning.plist

    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
    	<key>KeepAlive</key>
    	<true/>
    	<key>Label</key>
    	<string>org.generalov.isCharlesRunning</string>
    	<key>Program</key>
    	<string>/usr/local/bin/isCharlesRunning</string>
    	<key>RunAtLoad</key>
    	<true/>
    	<key>StandardErrorPath</key>
    	<string>/usr/local/var/log/org.generalov.isCharlesRunning.stderr.log</string>
    	<key>StandardOutPath</key>
    	<string>/usr/local/var/log/org.generalov.isCharlesRunning.stdout.log</string>
    </dict>
    </plist>
    
    EOF

## Uninstall

- stop service via `launchctl unload -w ~/Library/LaunchAgents/org.generalov.isCharlesRunning.plist`
- `rm ~/Library/LaunchAgents/org.generalov.isCharlesRunning.plist`
- `rm /usr/local/bin/isCharlesRunning`
